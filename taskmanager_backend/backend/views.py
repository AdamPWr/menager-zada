from django.shortcuts import render
from rest_framework import viewsets
from .models import User
from.models import Task
from .serializers import UserSerializer
from.serializers import TaskSerializer
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response


# Create your views here.

class UserView(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class TaskView(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

class AuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'id'   : user.id
        })

class UserTasks(viewsets.ViewSet):


    def list(self, request, pk=None):
        print(request.GET.get("id"))
        queryset = Task.objects.filter(assignedPerson_id = request.GET.get("id"))
        serializer = TaskSerializer(queryset, many=True)
        return Response(serializer.data)

