
from django.urls import path ,include
from . import views
from rest_framework import routers
from rest_framework.authtoken import views as rest_views


router = routers.DefaultRouter()
router.register('users', views.UserView)
router.register('tasks', views.TaskView)
router.register('user_tasks', views.UserTasks, basename = '/')
#router.register('api-token', rest_views.obtain_auth_token, name = 'api-token')

urlpatterns = [
    path('',include(router.urls)),
    # path('api-token/', rest_views.obtain_auth_token, name = 'authXDDD') AuthToken
    path('api-token/', views.AuthToken.as_view(), name = 'authXDDD')
]
