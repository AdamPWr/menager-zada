from rest_framework import serializers
from.models import User
from .models import Task

# disply User serilizer
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields  = ('id','login','password','name','surname','emplancment','team')

class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields  = ('deadline','assignedPerson','done','prioriry','toTo')
