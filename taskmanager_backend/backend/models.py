
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
# Create your models here.

class Team(models.Model):
    name = models.CharField(max_length = 20)

    def __str__(self):
        return self.name



class MyUserManager(BaseUserManager):
    def create_user(self, login, password=None):
        if not login:
            raise ValueError("Users must have a login!")

        user = self.model(
            login=login
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, login, password):
        user = self.create_user(
            login=login,
            password=password
        )

        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    EMPLACEMENT = [
        ('wr','worker'),
        ('mg','manager')
    ]

    login = models.CharField(max_length = 20, unique = True)
    # password = models.CharField(max_length = 20)
    name = models.CharField(max_length = 20)
    surname = models.CharField(max_length = 20)
    emplancment = models.CharField(choices = EMPLACEMENT, max_length = 2)
    team = models.ForeignKey(Team, on_delete =models.CASCADE, null = True)

    # Required for the AbstractBaseUser class
    date_joined = models.DateTimeField(verbose_name='date joined', auto_now_add=True)
    last_login = models.DateTimeField(verbose_name ='last login', auto_now=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    USERNAME_FIELD = 'login'
    REQUIRED_FIELDS = ['password']


    objects = MyUserManager()

    def __str__(self):
        return self.login

    # Required for the AbstractBaseUser class
    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True

class Task(models.Model):

    PRIORITY = [
        ('l','low'),
        ('m','medium'),
        ('u', 'urgent')
    ]

    deadline =  models.DateField()
    assignedPerson = models.ForeignKey(User, on_delete =models.CASCADE)
    done = models.BooleanField()
    prioriry = models.CharField(choices = PRIORITY, max_length = 1)
    toTo = models.TextField(null=True)


