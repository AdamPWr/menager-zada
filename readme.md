# Sprawozdanie overleaf

https://www.overleaf.com/project/5e751ea94b0d3600011d2305

# Podsumowania overleaf

## 1 Etap

https://www.overleaf.com/2125954791bcpwdbrwqtbf

# Instalacja

instalacja android-studio
- instalacja fluttera ( sprawdźcie czy wszystko działa za pomocą flutter doctor)
    -instrukcja do Windowsa https://flutter.dev/docs/get-started/install/windows
- instalacja venva
- instalacja requirements.txt na venvie
- instalacja postman ( apka do testowania api)

## Uruchamianie na Visual Studio Code na Windowsie

### Flutter

Ctrl+Shif+P -> Flutter: lunch emulator

Potem w konsoli:
```
adb reverse tcp:8000 tcp:8000
flutter run
```

### Django

Z poziomu głównego katalogu:
```
. venv/Scripts/activate
python taskmanager_backend/manage.py runserver
```

# Opis

Aplikacja powinna wspierać odbiorcę (w szczególności osoby pełniące funkcje kierownicze) w zarządzaniu własnymi zadaniami oraz ich delegowaniu. Duży nacisk przy tworzeniu będzie postawiony na UX i UI aby umożliwić jak najbardziej intuicyjną jej obsługę oraz funkcjonalności najlepiej realizujące optymalizację zarządzania zadaniami.
Aplikacja powinna działać zarówno na iOS jak i Android oraz jako aplikacja webowa (do opracowania w późniejszym etapie) .

    Podstawowe funkcje MVP:

    Zarządzanie zadaniami (dodawanie, usuwanie, modyfikacja, priorytetyzowanie, tagowanie, datowanie, raportowanie wykonania).
    Delegowanie zadań (przydzielanie, datowanie, rozliczanie)

    Dodatkowe funkcje:

    Implementacja funkcji zwiększających produktywność (m.in. geolokalizacja zadań, funkcja pomodoro)
    Śledzenie czasu pracy
    Grupowanie zadań w projekty i zarządzanie projektami.

Projekt realizowany będzie zgodnie z metodyką Scrum. Technologie do wykorzystania dowolne według uznania zespołu projektowego. Aplikacja powstaje w celu jej komercyjnego wykorzystania na rynku. Najaktywniejszym osobom z zespołu zaproponowana zostanie możliwość dalszej pracy nad produktem oraz udział w zyskach ze sprzedaży.
Ze wszystkimi osobami wchodzącymi w skład zespołu podpisana zostanie umowa praktyk/staży. Firma wymaga przekazania majątkowych praw autorskich