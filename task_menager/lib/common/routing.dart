import 'package:flutter/material.dart';
import 'package:task_menager/screens/home.dart';
import 'package:task_menager/screens/Login.dart';
import 'package:task_menager/screens/AddTaskScreen.dart';
import 'package:task_menager/screens/taskDetailsScreen.dart';
import 'package:task_menager/common/argumentsClass.dart';

class RouteGenerator {

  static Route<dynamic> generteRoute(RouteSettings settings){

    final args = settings.arguments;

    switch(settings.name){
      case '/':
        return MaterialPageRoute(builder: (_)=>LoginScreen());

      case '/home':
        {
          if(args is LoginResponse)
            print("user id w generatorzeRout ${args.userId}");
          return MaterialPageRoute(builder: (_)=>Home(args));
          break;
        }
      case '/addTaskScreen':
        return MaterialPageRoute(builder: (_)=>AddTaskScreen());
      case '/taskDetailsScreen':
        {
          if(args is Map)
            {
              return MaterialPageRoute(builder: (_)=>TaskDetailScreen(args));
            }
        }

    }
  }

}