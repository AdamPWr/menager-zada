import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:task_menager/common/TaskTile.dart';


class TaskViewContainer extends StatefulWidget {

  List tasks = new List();

  TaskViewContainer(@required this.tasks);

  void refresh()
  {
  }

  @override
  _TaskViewContainerState createState() => _TaskViewContainerState();

}

class _TaskViewContainerState extends State<TaskViewContainer> {

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: ListView.builder(
        itemCount: widget.tasks.length,
          itemBuilder: (BuildContext ctxt, int index) {
            return new TaskTile(task : widget.tasks[index]);
          }),
    );
  }
}
