import 'package:flutter/material.dart';

class TaskTile extends StatelessWidget {

  Map task;

  TaskTile({Key key, @required this.task}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: Icon(
            Icons.event,
          color: Colors.purple,
        ),
        onTap: (){
          Navigator.pushNamed(context, '/taskDetailsScreen', arguments: task);
        },
        title: Text(task['toTo']),
        

      ),
      elevation: 20,
      color: Colors.grey[800],
    );
  }
}

