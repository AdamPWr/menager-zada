import 'package:flutter/material.dart';


class TeamMemberTile extends StatelessWidget {

  String imie,nazwisko;

  TeamMemberTile({Key key, @required this.imie, @required this.nazwisko}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(

        leading: Icon(
          Icons.supervised_user_circle,
          color: Colors.purple,
        ),
        onTap: (){
          //TODO
        },
        title: Text("Imie: $imie \nNazwisko: $nazwisko"),


      ),
      elevation: 20,
      color: Colors.grey[850],
    );
  }
}
