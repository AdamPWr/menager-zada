import 'package:flutter/material.dart';

class SwitchCardButton extends StatefulWidget {

  final String label;

  VoidCallback callback;

  SwitchCardButton({Key key, @required this.label, @required this.callback}) : super(key: key);
  @override
  _SwitchCardButtonState createState() => _SwitchCardButtonState();
}

class _SwitchCardButtonState extends State<SwitchCardButton> {


  @override
  Widget build(BuildContext context) {


    return InkWell(

      onTap: (){

        widget.callback();
      },
      child: Ink(
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.4),
              spreadRadius: 1,
              blurRadius: 2,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
          gradient: LinearGradient(colors: <Color>[Colors.grey[800],Colors.grey[700]]),
        ),
        child: Container(
          alignment: Alignment.center,
          constraints: BoxConstraints.expand(),
          child: Text(
            widget.label,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 20,
              letterSpacing: 2.0,
              fontWeight: FontWeight.bold,
              color: Colors.purple[900]
            ),

          ),
          ),
        ),
      );
  }
}

