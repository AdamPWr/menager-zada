import 'package:flutter/material.dart';


class LoginResponse
{
  int userId;
  String token;

  LoginResponse(@required this.userId, @required this.token);
}