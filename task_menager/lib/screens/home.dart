import 'package:flutter/material.dart';
import 'package:task_menager/screens/homeScreen.dart';
import 'package:task_menager/screens/taskScreen.dart';
import 'package:task_menager/screens/teamScreen.dart';
import 'package:task_menager/common/argumentsClass.dart';

class Home extends StatefulWidget {

  final LoginResponse args;
  Home(this.args);

  @override
  _HomeState createState() => _HomeState();

}

class _HomeState extends State<Home> {

  int currentIndex = 0;





  @override
  Widget build(BuildContext context) {

    final  List<Widget> widgets = [HomeScreen(),teamScreen(),taskScreen(widget.args.userId)];
//    UserId userId = ModalRoute.of(context).settings.arguments;

    print("User id w homie ${widget.args.userId} ");

    return Scaffold(
      backgroundColor: Colors.grey[900],
      body: widgets[currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.black26,
        onTap: (index){
          setState(() {
            currentIndex = index;
          });
          print("CUrrent index = $currentIndex");
        },
        currentIndex: currentIndex,
        unselectedItemColor: Colors.green,
        selectedItemColor: Colors.green[700],
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.home, color: Colors.purple,),
              title: Text("Home")
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.supervised_user_circle , color: Colors.purple,),
              title: Text("Team")
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.clear_all , color: Colors.purple,),
              title: Text("Tasks")
          ),
        ],
      ),
    );
  }
}
