import 'package:flutter/material.dart';
import 'package:task_menager/common/SwitchTaskViewButton.dart';
import 'package:task_menager/common/taskViewContainer.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class taskScreen extends StatefulWidget {
  @override
  _taskScreenState createState() => _taskScreenState();

  int userId;
  taskScreen(this.userId);

}

class _taskScreenState extends State<taskScreen> {

  List allTasksList = List();
  List myTasksList = List();
  List<TaskViewContainer> taskViewContainer = [];

  int currentDisplayedTasksListIndex = 0;


  Future gettingTaks;

  @override
  void initState()
  {
    super.initState();
    gettingTaks =  getAllTasksList();

  }


  Future<bool> getAllTasksList() async
  {
    var url = 'http://localhost:8000/tasks';

    var response = await http.get(url, headers: {"Authorization" : "Token 53fdbe35207f873e10405d3f928a168a4196459e"});


    print('Response status w taskScreenie: ${response.statusCode}');
    //print('Response body: ${response.body}');

    if(response.statusCode == 200)
    {
      allTasksList = jsonDecode(response.body);
      print(response.body);
    }
    else
    {
      print("ERROR w try getAllTasksList");
      //return false;
      return true;
    }
    return true;
  }

  @override
  Widget build(BuildContext context)   {


    void myTasksCallback(){
      print("Zmiana widoku taskow na myTasks");
      //taskViewContainer[1] = TaskViewContainer(myTasksList);

      setState(() {
        currentDisplayedTasksListIndex =1;
      });

    };
    void allTasksCallback(){
      print("Zmiana widoku taskow na allTasks");
      //taskViewContainer[0] = TaskViewContainer(allTasksList);
      setState(() {
        currentDisplayedTasksListIndex =0;
      });
    };



    return Scaffold(
      backgroundColor: Colors.grey[900],
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.deepPurple,
        elevation: 0.0,

        child: Icon(Icons.add, color: Colors.green,),
        onPressed: (){
          Navigator.pushNamed(context, '/addTaskScreen');
        },
      ),
      body: Column(

        children: [
          Expanded(
            flex: 2,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Expanded(
                    child: SwitchCardButton(label : "My tasks",callback: myTasksCallback)
                ),
                SizedBox(width: 5,),
                Expanded(
                  child: SwitchCardButton(label: "All tasks", callback: allTasksCallback),
                  //child: SwitchCardButton(),
                )
              ],
            ),
          ),
          FutureBuilder(
            future: gettingTaks,
            builder: (context,snapshot){
              if(snapshot.connectionState == ConnectionState.done)
                {
                  print("odpalił sie conn state = done");
                  myTasksList = allTasksList.where((element) {
                    if(element['assignedPerson'] == widget.userId)
                    {
                      print("Assigned persen ${element['assignedPerson']}");
                      return true;
                    }
                    else
                    {
                      print("Assigned persen else ${element['assignedPerson']}");
                      return false;
                    }
                  }).toList();
                  print("Moje zadania: $myTasksList");
                  print("all task size = ${allTasksList.length}");
                  print("My task size = ${myTasksList.length}");
                  taskViewContainer.add(TaskViewContainer(allTasksList));
                  taskViewContainer.add(TaskViewContainer(myTasksList));
                  return Expanded(
                    flex: 10,
                    child: taskViewContainer[currentDisplayedTasksListIndex],
                  );
                }
              else
                {
                  print("odpaliła sie alternatywa connetion state");
                  return Expanded(flex: 10,
                    child: Container(
                      child: Text(
                        //"Pewnie nie zalaczyles serwera",
                        "",
                        style: TextStyle(
                          letterSpacing: 2,
                          fontSize: 20
                        ),
                      ),
                    ),
                  ); //returning blank container
                  //Expanded(child: CircularProgressIndicator());
                }
            },
          ),
        ],
      ),
    );
  }
}
