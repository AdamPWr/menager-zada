import 'package:flutter/material.dart';

class TaskDetailScreen extends StatelessWidget {

  Map task;

  TaskDetailScreen(this.task);

  @override
  Widget build(BuildContext context) {

    String priority;

    switch (task['prioriry'])
    {
      case 'l':
        {
          priority = "Low";
          break;
        }
      case 'm':
        {
          priority = "Medium";
          break;
        }
      case 'u':
        {
          priority = "Urgent";
          break;
        }
    }

    return Scaffold(
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        title: Text("Task Details",
          style: TextStyle(
              letterSpacing: 1,
              color: Colors.purple[700]
          ),
        ),
        backgroundColor: Colors.grey[900],

      ),

      body: Column(


        children: [
          SizedBox(height: 30,),
          Card(
            child: ListTile(
              leading: Icon(
                Icons.account_circle,
                color: Colors.purple,
              ),
              onTap: (){
                // TODO
              },
              title: Text("Asigned person: Adam Cierniak"),


            ),
            elevation: 20,
            color: Colors.grey[800],
          ),
          Card(
            child: ListTile(
              leading: Icon(
                Icons.priority_high,
                color: Colors.purple,
              ),
              onTap: (){
                // TODO
              },
              title: Text("Priority: $priority"),


            ),
            elevation: 20,
            color: Colors.grey[800],
          ),
          Card(
            child: ListTile(
              leading: Icon(
                Icons.calendar_today,
                color: Colors.purple,
              ),
              onTap: (){
                // TODO
              },
              title: Text("Deadline: ${task['deadline']}"),


            ),
            elevation: 20,
            color: Colors.grey[800],
          ),
          Card(
            child: ListTile(
              leading: Icon(
                Icons.chevron_right,
                color: Colors.purple,
              ),
              onTap: (){
                // TODO
              },
              title: Text("To do: ${task['toTo']}"),


            ),
            elevation: 20,
            color: Colors.grey[800],
          ),
          SizedBox(height: 20,),
          RaisedButton(
            onPressed: ()async  {
              Navigator.pop(context);
            },
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
            padding: const EdgeInsets.all(0.0),
            child: Ink(
              decoration: const BoxDecoration(
                gradient: LinearGradient(colors: <Color>[
                  Colors.purple,
                  Color(0xFF42A5F5),
                ]),
                borderRadius: BorderRadius.all(Radius.circular(80.0)),
              ),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.60,
                constraints: const BoxConstraints(minWidth: 88.0, minHeight: 50.0), // min sizes for Material buttons
                alignment: Alignment.center,
                child: const Text(
                  'Mark as done',
                  textAlign: TextAlign.center,
                ),
              ),
            ),

          )
        ],

      ),
    );
  }
}
