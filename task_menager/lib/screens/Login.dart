
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:task_menager/common/argumentsClass.dart';


class LoginScreen extends StatelessWidget {


  String token;
  int userId;
  String login = 'root';
  String password = 'suser';

  TextEditingController loginControler = new TextEditingController();
  TextEditingController passwordControler = new TextEditingController();

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  Future<bool> tryLoginAndGetToken() async
  {
    var url = 'http://127.0.0.1:8000/api-token/';
    print('Login $login');
    print(password);
    var response = await http.post(url, body: {'username': "$login", 'password': "$password"});


    print('Response status: ${response.statusCode}');
   // print('Response body: ${response.body}');

    if(response.statusCode == 200)
    {
      Map data = jsonDecode(response.body);
      token = data["token"];
      userId = data["id"];
      print('token w loginie $token');
      print('use id  w loginie $userId');
    }
    else
      {
        print("ERROR w try LoginGetToken");
        //return false;
        return true;
      }
    return true;
  }


  @override
  Widget build(BuildContext context) {
  print("DUPA999");
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        color: Colors.grey[900],
        child: Column(
            children: <Widget>[
              Flexible(
                flex: 3,
                child: ClipPath(
                  clipper: Clipper(),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[800],
                      image: DecorationImage(
                        image: AssetImage("assets/logo.png"),
                      )
                    ),
                  ),
                ),
              ),
              Flexible(
                flex: 4,
                child: Container(
                  color: Colors.grey[890],
                  child: Padding(
                    padding: EdgeInsets.all(40),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        TextFormField(
                          controller: loginControler,
                          obscureText: false,
                          decoration: InputDecoration(
                              fillColor: Colors.grey[850],
                              labelText: "Login",
                              filled: true,
                              border: OutlineInputBorder(
                                borderRadius: const BorderRadius.all(

                                  const Radius.circular(8.0),
                                ),
                                borderSide: new BorderSide(
                                  color: Colors.transparent,
                                  width: 1.0,
                                ),
                              )
                          ),
                        ),
                        SizedBox(height: 10,),
                        TextFormField(
                          controller: passwordControler,
                          obscureText: true,
                          decoration: InputDecoration(
                            fillColor: Colors.grey[850],
                            labelText: "Password",
                            filled: true,
                            border: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(

                                const Radius.circular(8.0),
                              ),
                              borderSide: new BorderSide(
                                color: Colors.transparent,
                                width: 1.0,
                              ),
                            )
                          ),
                        ),
                        SizedBox(height: 10,),
                        RaisedButton(
                          onPressed: ()async  {
                            login = loginControler.text;
                            password = passwordControler.text;
                            print("login in button func: $login");
                            print("password in button func: $password");
                            var succes = await tryLoginAndGetToken();
                            if(succes)
                              {
                                print("Przekazywanie do home user id $userId");
                                Navigator.pushReplacementNamed(context, '/home', arguments: LoginResponse(userId,token));
                              }
                            else
                              _scaffoldKey.currentState.showSnackBar(SnackBar(
                                content: Text("Login Failed",
                                style: TextStyle(
                                  color: Colors.deepPurple,
                                  fontSize: 20
                                ),),
                              ));
                          },
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
                          padding: const EdgeInsets.all(0.0),
                          child: Ink(
                            decoration: const BoxDecoration(
                              gradient: LinearGradient(colors: <Color>[
                                Colors.purple,
                                Color(0xFF42A5F5),
                              ]),
                              borderRadius: BorderRadius.all(Radius.circular(80.0)),
                            ),
                            child: Container(
                              constraints: const BoxConstraints(minWidth: 88.0, minHeight: 50.0), // min sizes for Material buttons
                              alignment: Alignment.center,
                              child: const Text(
                                'Login',
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),

                        )
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
      ),
    );
  }
}

class Clipper extends CustomClipper<Path>
{
  @override
  Path getClip(Size size) {

    var path = Path();

      path.lineTo(0.0, size.height - 20);

      var firstControlPoint = Offset(size.width / 4, size.height);
      var firstEndPoint = Offset(size.width / 2.25, size.height - 30.0);
      path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
          firstEndPoint.dx, firstEndPoint.dy);

      var secondControlPoint =
      Offset(size.width - (size.width / 3.25), size.height - 65);
      var secondEndPoint = Offset(size.width, size.height - 40);
      path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
          secondEndPoint.dx, secondEndPoint.dy);

      path.lineTo(size.width, size.height - 40);
      path.lineTo(size.width, 0.0);
      path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return true;
  }

}
