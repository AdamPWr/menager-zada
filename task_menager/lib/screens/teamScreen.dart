import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:task_menager/common/teamMemberTile.dart';

class teamScreen extends StatefulWidget {
  @override
  _teamScreenState createState() => _teamScreenState();
}

class _teamScreenState extends State<teamScreen> {

  String nameOfTheTeam = "Chujinski team";

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        //mainAxisAlignment: MainAxisAlignment.center,
        //crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox( height: MediaQuery.of(context).size.height * 0.03,),
          Center(
            child: Container(
              width: MediaQuery.of(context).size.width * 0.90,
              height: MediaQuery.of(context).size.height * 0.10,
              decoration: BoxDecoration(
                  color: Colors.grey[800],
                  borderRadius: BorderRadius.circular(20)
              ),
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Row(
                  children: [
                    Icon(Icons.supervised_user_circle, color: Colors.purple,),
                    SizedBox(width: MediaQuery.of(context).size.width * 0.01,),
                    Text(
                      "Team's name: $nameOfTheTeam",
                      style: TextStyle(
                        letterSpacing: 2.0,
                        fontWeight: FontWeight.bold,

                      ),
                    )
                  ],
                ),
              ),

            ),
          ),
          SizedBox( height: MediaQuery.of(context).size.height * 0.03,),
          Container(
            width: MediaQuery.of(context).size.width * 0.90,
            height: MediaQuery.of(context).size.width * 0.97,
            decoration: BoxDecoration(
                color: Colors.grey[800],
                borderRadius: BorderRadius.circular(20)
            ),
            child: Column(

              children: [
                Text(
                    "Members",
                style: TextStyle(
                  fontSize: 20
                ),
                ),
                SizedBox( height: MediaQuery.of(context).size.height * 0.03,),
                Column(
                  children: [
                    TeamMemberTile(imie: "Adam",nazwisko: "Cierniak",),
                    TeamMemberTile(imie: "Marek",nazwisko: "Choinski",),
                    TeamMemberTile(imie: "Paweł",nazwisko: "Pogudz",),
                    TeamMemberTile(imie: "Kuba",nazwisko: "Horochowski",),


                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
