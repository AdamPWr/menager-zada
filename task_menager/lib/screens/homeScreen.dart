import 'package:flutter/material.dart';
import 'dart:async';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int numberOfTasks = 5;
  List<String> buttonLabel = ["Start", "Stop"];



  Stopwatch watch = Stopwatch();
  Timer timer;
  bool startStop = true;

  String elapsedTime = '00:00:00';

  updateTime(Timer timer) {
    if (watch.isRunning) {
      setState(() {
        print("startstop Inside=$startStop");
        elapsedTime = transformMilliSeconds(watch.elapsedMilliseconds);
      });
    }
  }

  var buttonLabelIndex = 0;
  @override
  Widget build(BuildContext context) {

    final int userId = ModalRoute.of(context).settings.arguments;

    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.90,
                height: MediaQuery.of(context).size.height * 0.10,
                decoration: BoxDecoration(
                    color: Colors.grey[800],
                    borderRadius: BorderRadius.circular(20)),
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Row(
                    children: [
                      Icon(
                        Icons.report_problem,
                        color: Colors.purple,
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.08,
                      ),
                      Text(
                        "Tasks to be done: $numberOfTasks",
                        style: TextStyle(
                          letterSpacing: 2.0,
                          fontWeight: FontWeight.bold,
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.05,
          ),
          Container(
              height: MediaQuery.of(context).size.height * 0.50,
              width: MediaQuery.of(context).size.width * 0.90,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.grey[800]),
              child: Column(
                children: [
                  Expanded(
                    child: Center(
                        child: Text(
                      "Working time",
                      style: TextStyle(
                        letterSpacing: 2.0,
                        fontWeight: FontWeight.bold,
                      ),
                    )),
                    flex: 1,
                  ),
                  Expanded(
                    child: Center(
                      child: Text(
                        elapsedTime,
                        style:
                            TextStyle(color: Colors.purple[900], fontSize: 80),
                      ),
                    ),
                    flex: 4,
                  )
                ],
              )),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.05,
          ),
          RaisedButton(
            onPressed: () async {
              startOrStop();

              if (buttonLabelIndex == 0) {
                buttonLabelIndex = 1;
              } else {
                buttonLabelIndex = 0;
              }
              setState(() {});
            },
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(80.0)),
            padding: const EdgeInsets.all(0.0),
            child: Ink(
              decoration: const BoxDecoration(
                gradient: LinearGradient(colors: <Color>[
                  Colors.purple,
                  Color(0xFF42A5F5),
                ]),
                borderRadius: BorderRadius.all(Radius.circular(80.0)),
              ),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.60,
                constraints: const BoxConstraints(
                    minWidth: 88.0,
                    minHeight: 50.0), // min sizes for Material buttons
                alignment: Alignment.center,
                child: Text(
                  buttonLabel[buttonLabelIndex],
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      letterSpacing: 3, fontSize: 20, color: Colors.black26),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  startOrStop() {
    if (startStop) {
      startWatch();
    } else {
      stopWatch();
    }
  }

  startWatch() {
    setState(() {
      startStop = false;
      watch.start();
      timer = Timer.periodic(Duration(milliseconds: 100), updateTime);
    });
  }

  stopWatch() {
    setState(() {
      startStop = true;
      watch.stop();
      setTime();
    });
  }

  setTime() {
    var timeSoFar = watch.elapsedMilliseconds;
    setState(() {
      elapsedTime = transformMilliSeconds(timeSoFar);
    });
  }

  transformMilliSeconds(int milliseconds) {
    int hundreds = (milliseconds / 10).truncate();
    int seconds = (hundreds / 100).truncate();
    int minutes = (seconds / 60).truncate();
    int hours = (minutes / 60).truncate();

    String hoursStr = (hours % 60).toString().padLeft(2, '0');
    String minutesStr = (minutes % 60).toString().padLeft(2, '0');
    String secondsStr = (seconds % 60).toString().padLeft(2, '0');

    return "$hoursStr:$minutesStr:$secondsStr";
  }
}
