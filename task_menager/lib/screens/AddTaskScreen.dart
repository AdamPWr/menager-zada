import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

class AddTaskScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        title: Text("Add new task",
        style: TextStyle(
          letterSpacing: 1,
          color: Colors.purple[700]
        ),
        ),
        backgroundColor: Colors.grey[900],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextFormField(

              decoration: InputDecoration(
                  fillColor: Colors.purpleAccent,
                labelText: "To do"
              ),
            ),
            SizedBox(height: 5,),
            TextFormField(

              decoration: InputDecoration(
                  fillColor: Colors.purpleAccent,
                  labelText: "Assigned Person"
              ),
            ),
            SizedBox(height: 5,),
            TextFormField(

              decoration: InputDecoration(
                  fillColor: Colors.purpleAccent,
                  labelText: "Prioryty"
              ),
            ),
            SizedBox(height: 5,),
            TextFormField(

              decoration: InputDecoration(
                  fillColor: Colors.purpleAccent,
                  labelText: "Deadline"
              ),
            ),
            SizedBox(height: 15,),
            RaisedButton(
              onPressed: ()async  {
                Navigator.pop(context);
              },
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
              padding: const EdgeInsets.all(0.0),
              child: Ink(
                decoration: const BoxDecoration(
                  gradient: LinearGradient(colors: <Color>[
                    Colors.purple,
                    Color(0xFF42A5F5),
                  ]),
                  borderRadius: BorderRadius.all(Radius.circular(80.0)),
                ),
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.60,
                  constraints: const BoxConstraints(minWidth: 88.0, minHeight: 50.0), // min sizes for Material buttons
                  alignment: Alignment.center,
                  child: const Text(
                    'Add',
                    textAlign: TextAlign.center,
                  ),
                ),
              ),

            )
          ],
        ),
      ),
    );
  }
}
